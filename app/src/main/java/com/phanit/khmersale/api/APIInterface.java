package com.phanit.khmersale.api;


import com.phanit.khmersale.models.Article;
import com.phanit.khmersale.models.BaseResponse;
import com.phanit.khmersale.models.Category;
import com.phanit.khmersale.models.CategoryRes;
import com.phanit.khmersale.models.Slide;
import com.phanit.khmersale.models.auth.LoginReq;
import com.phanit.khmersale.models.auth.LoginRes;
import com.phanit.khmersale.models.auth.Register;
import com.phanit.khmersale.models.auth.RegisterRes;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface APIInterface {

    @POST("/api/app/oauth/token")
    Call<LoginRes> log(@Body LoginReq req);

    @POST("/api/app/oauth/register")
    Call<RegisterRes> register(@Body Register req);

    @POST("/api/public/image/upload")
    @Multipart
    Call<BaseResponse<String>> uploadImage(@Part MultipartBody.Part part);

//    @GET("/app/api/category/list")
//    Call<CategoryRes> getAllCategories();

    @GET("/api/public/category/list")
    Call<BaseResponse<List<Category>>> getAllCategoryWithBase();



    @POST("/api/public/category/create")
    Call<BaseResponse<String>> createCategory(@Body Category req);

    @GET("/api/public/category/{id}")
    Call<BaseResponse<Category>> getCategoryById(@Path("id") int id);

    @POST("/api/public/category/update")
    Call<BaseResponse<String>> updateCategory(@Body Category req);

    @POST("/api/public/category/delete")
    Call<BaseResponse<String>> deleteCategory(@Body Category req);


    @GET("/api/public/article/list")
    Call<BaseResponse<List<Article>>> getAllArticle();

    @POST("/api/public/article/create")
    Call<BaseResponse<String>> createArticle(@Body Article req);

    @GET("/api/public/article/{id}")
    Call<BaseResponse<Article>> getArticleById(@Path("id") int id);

    @POST("/api/public/article/update")
    Call<BaseResponse<String>> updateArticle(@Body Article req);

    @GET("/api/public/article/category/{id}")
    Call<BaseResponse<List<Article>>> getArticleByCategoryId(@Path("id") int id);

    @GET("/api/public/app/image/list")
    Call<BaseResponse<List<Slide>>> getImageList();
}
