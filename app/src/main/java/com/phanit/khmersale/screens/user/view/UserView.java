package com.phanit.khmersale.screens.user.view;

import com.phanit.khmersale.models.Article;
import com.phanit.khmersale.views.BaseView;


public interface UserView extends BaseView {
    void onLoading();

    void onHiding();

    void onError(String message);

    void onSuccess(Object data);

    void onGetArticleByCategoryId(Object data);

    void onGetArticleByIdSuccess(Article article);
}
