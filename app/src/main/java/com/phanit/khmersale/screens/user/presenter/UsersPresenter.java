package com.phanit.khmersale.screens.user.presenter;

import androidx.annotation.NonNull;

import com.phanit.khmersale.api.APIClient;
import com.phanit.khmersale.api.APIInterface;
import com.phanit.khmersale.models.Article;
import com.phanit.khmersale.models.BaseResponse;
import com.phanit.khmersale.screens.user.view.UserView;


import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UsersPresenter {

    private UserView view;
    private APIInterface apiInterface;

    public UsersPresenter(UserView view) {
        this.view = view;
        apiInterface = APIClient.getClientAPIs().create(APIInterface.class);
    }

    public void getArticleByCategoryId(int id){
        view.onLoading();
        apiInterface.getArticleByCategoryId(id).enqueue(new Callback<BaseResponse<List<Article>>>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse<List<Article>>> call, @NonNull Response<BaseResponse<List<Article>>> response) {
                view.onHiding();
                if (response.isSuccessful() && response.body() != null){
                    view.onGetArticleByCategoryId(response.body().getData());
                }
            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse<List<Article>>> call, @NonNull Throwable t) {
                view.onHiding();
                view.onError(t.getLocalizedMessage());
            }
        });
    }

    public void getArticleId(int id){
        view.onLoading();
        apiInterface.getArticleById(id).enqueue(new Callback<BaseResponse<Article>>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse<Article>> call, @NonNull Response<BaseResponse<Article>> response) {
                view.onHiding();
                if (response.isSuccessful() && response.body() != null){
                    view.onGetArticleByIdSuccess(response.body().getData());
                }
            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse<Article>> call, @NonNull Throwable t) {
                view.onHiding();
                view.onError(t.getLocalizedMessage());
            }
        });
    }

}
