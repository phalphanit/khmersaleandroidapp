package com.phanit.khmersale.screens;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.phanit.khmersale.MainActivity;
import com.phanit.khmersale.R;
import com.phanit.khmersale.app.BaseActivity;
import com.phanit.khmersale.screens.authentication.LoginActivity;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        // Add a delay using a Handler to display the splash screen for a specific duration
        new Handler().postDelayed(() -> {

            // Start the main activity and finish the splash activity
            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }, 3000); // Delay duration in milliseconds (e.g., 2000 = 2 seconds)
    }
}