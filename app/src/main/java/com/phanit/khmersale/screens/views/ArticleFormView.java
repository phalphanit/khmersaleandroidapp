package com.phanit.khmersale.screens.views;
import com.phanit.khmersale.models.Article;
import com.phanit.khmersale.models.Category;
import com.phanit.khmersale.views.BaseView;

import java.util.List;

public interface ArticleFormView extends BaseView {
    void onGetAllCategorySuccess(List<Category> list);
    void onGetArticleIdSuccess(Article article);

    void onUploadImageSuccess(String imageUrl);
}
