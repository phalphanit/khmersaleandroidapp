//package com.phanit.khmersale.screens.user;
//
//import android.os.Bundle;
//
//import androidx.viewpager2.widget.ViewPager2;
//
//import com.google.android.material.tabs.TabLayout;
//import com.phanit.khmersale.R;
//import com.phanit.khmersale.adapter.MyViewPagerAdapter;
//import com.phanit.khmersale.app.BaseActivity;
//
//import java.util.Objects;
//
//public class TabViewActivity extends BaseActivity {
//    TabLayout tabLayout;
//    ViewPager2 viewPager2;
//    MyViewPagerAdapter myViewPagerAdapter;
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_tab_view);
//        initBack();
//        initAppBar("SABAY NEWS");
//
//        tabLayout = findViewById(R.id.tab_layout);
//        viewPager2 =findViewById(R.id.view_pager);
//        myViewPagerAdapter = new MyViewPagerAdapter(this);
//        viewPager2.setAdapter(myViewPagerAdapter);
//
//        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
//            @Override
//            public void onTabSelected(TabLayout.Tab tab) {
//                viewPager2.setCurrentItem(tab.getPosition());
//            }
//
//            @Override
//            public void onTabUnselected(TabLayout.Tab tab) {
//
//            }
//
//            @Override
//            public void onTabReselected(TabLayout.Tab tab) {
//
//            }
//        });
//
//        viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
//            @Override
//            public void onPageSelected(int position) {
//                super.onPageSelected(position);
//                Objects.requireNonNull(tabLayout.getTabAt(position)).select();
//            }
//        });
//    }
//}