package com.phanit.khmersale.screens.user;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.phanit.khmersale.R;
import com.phanit.khmersale.adapter.ArticleCategoryListAdapter;
import com.phanit.khmersale.app.BaseActivity;
import com.phanit.khmersale.callback.CallBackListener;
import com.phanit.khmersale.constants.Constant;
import com.phanit.khmersale.models.Article;
import com.phanit.khmersale.models.Category;
import com.phanit.khmersale.screens.user.presenter.UsersPresenter;
import com.phanit.khmersale.screens.user.view.UserView;
import com.phanit.khmersale.screens.views.ArticleView;

import java.util.List;

public class CategoryArticleList extends BaseActivity implements UserView {

    private RecyclerView recyclerView;
    private UsersPresenter presenter;
    private ArticleCategoryListAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_article_list);
        initProgressBar();
        initView();
        initBack();
        initSwipeRefresh();

    }

    private void initView() {
        recyclerView = findViewById(R.id.rc_category_article_list);
        progressBar = findViewById(R.id.ProgressBar);
        swipeRefreshLayout = findViewById(R.id.swipeRefresh);


        Intent intent = getIntent();
        presenter = new UsersPresenter(this);

        int id = intent.getIntExtra(Constant.ID,0);
        String categoryName = intent.getStringExtra(Constant.NAME);
        initAppBar(categoryName);
        if (id != 0){
            presenter.getArticleByCategoryId(id);
        }
        swipeRefreshLayout.setOnRefreshListener(() -> {
            presenter.getArticleByCategoryId(id);
            swipeRefreshLayout.setRefreshing(false);
        });
    }


    @Override
    public void onLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHiding() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onSuccess(Object data) {

    }

    @Override
    public void onGetArticleByCategoryId(Object data) {
        List<Article> list = (List<Article>) data;
        adapter = new ArticleCategoryListAdapter(this, list, new CallBackListener() {
            @Override
            public void onClickCardItem(Object data, View view) {
                Article article = (Article) data;
                Intent intent = new Intent(CategoryArticleList.this, ArticleDetailsActivity.class);
                intent.putExtra(Constant.ID,article.getId());
                intent.putExtra(Constant.TITLE,article.getTitle());
                intent.putExtra(Constant.DESCRIPTION,article.getDescription());
                intent.putExtra(Constant.IMAGE_URL,article.getImageUrl());
                startActivity(intent);
            }
        });
        GridLayoutManager layoutManager = new GridLayoutManager(this,1,RecyclerView.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

    }

    @Override
    public void onGetArticleByIdSuccess(Article article) {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onServerError(String message) {

    }
}