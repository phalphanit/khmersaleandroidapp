package com.phanit.khmersale.screens.admin.category;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Option;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.model.Headers;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.phanit.khmersale.R;
import com.phanit.khmersale.app.BaseActivity;
import com.phanit.khmersale.constants.Constant;
import com.phanit.khmersale.models.Category;
import com.phanit.khmersale.models.User;
import com.phanit.khmersale.screens.presenters.CategoryFormPresenter;
import com.phanit.khmersale.screens.views.CategoryFormView;
import com.phanit.khmersale.utils.CheckRuntimePermission;
import com.phanit.khmersale.utils.local.UserSharedPreference;


public class CategoryFormActivity extends BaseActivity implements CategoryFormView {
    private EditText categoryName, categoryNameKh;
    private RadioButton active, delete;
    private Button btnCreate, btnDelete;
    private ImageView imageViewUpload;
    private Category category;
    private CategoryFormPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_form);
        initView();
        initProgressBar();
        initBack();
    }

    private void initView() {
        new CheckRuntimePermission().grantWriteExternalStorage(this);
        categoryName = findViewById(R.id.etCategoryName);
        categoryNameKh = findViewById(R.id.etCategoryNameKh);
        active = findViewById(R.id.rbActive);
        delete = findViewById(R.id.rbDelete);
        btnCreate = findViewById(R.id.btnCreateCategory);
        btnDelete = findViewById(R.id.btnDeleteCategory);
        imageViewUpload = findViewById(R.id.ivCategoryForm);

        presenter = new CategoryFormPresenter(this);
        category = new Category();

        Intent intent = getIntent();
        int id = intent.getIntExtra("ID", 0);
        if (id == 0) {
            initAppBar("Create");
            btnDelete.setVisibility(View.GONE);
        }
        if (id != 0) {
            initAppBar("Update");
            category.setId(intent.getIntExtra("ID", 0));
            category.setName(intent.getStringExtra("NAME"));
            category.setNameKh(intent.getStringExtra("NAME_KH"));
            category.setImageUrl(intent.getStringExtra("IMAGE"));
            category.setStatus(intent.getStringExtra("STATUS"));

            categoryName.setText(category.getName());
            categoryNameKh.setText(category.getNameKh());
            if (category.getStatus().equals("ACT")) {
                active.setChecked(true);
            } else {
                delete.setChecked(true);
            }
            btnCreate.setText(R.string.update);
        }

        imageViewUpload.setOnClickListener(view -> chooseImage());

    }

    @Override
    public void onLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHiding() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onSuccess(Object data) {

    }

    @Override
    public void onError(String message) {
        showMessage(message);
    }

    @Override
    public void onServerError(String message) {
        showMessage(message);
    }

    public void onClickCreateCategory(View view) {
        if (category != null) {
            category.setName(categoryName.getText().toString().trim());
            category.setNameKh(categoryNameKh.getText().toString().trim());
            if (active.isChecked()) {
                category.setStatus("ACT");
            } else {
                category.setStatus("DEL");
            }
            if (category.getId() == 0) {
                presenter.createCategory(category);
            } else {
                presenter.updateCategory(category);
            }
        }
        setResult(CategoryActivity.RESULT);
        finish();
    }

    public void onClickDeleteCategory(View view){
        if (category != null){
            category.setName(categoryName.getText().toString().trim());
            category.setNameKh(categoryNameKh.getText().toString().trim());
            category.setStatus(Constant.DEL);
            if (category.getId() != 0) {
                presenter.delCategory(category);
            }
        }
        setResult(CategoryActivity.RESULT);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK) {
                assert data != null;
                Uri uri = data.getData();
                try {
                    String[] filePathColumns = {MediaStore.Images.Media.DATA};
                    assert uri != null;
                    Cursor cursor = getContentResolver().query(uri, filePathColumns, null, null, null);
                    assert cursor != null;
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumns[0]);
                    String filePath = cursor.getString(columnIndex);
                    cursor.close();
                    presenter.uploadImage(filePath, this);
                } catch (Throwable e) {
                    showMessage(e.getMessage());
                }

            }
        } catch (Throwable e) {
            showMessage(e.getMessage());
        }
    }

    @Override
    public void onUploadImageSuccess(String imageUrl) {
        category.setImageUrl(imageUrl);
        Glide.with(this).load(Constant.BaseUrlImageView + imageUrl)
                .centerCrop()
                .into(imageViewUpload);
    }
}