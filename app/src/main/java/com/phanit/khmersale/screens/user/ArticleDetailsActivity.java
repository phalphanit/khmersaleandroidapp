package com.phanit.khmersale.screens.user;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.ImageView;

import com.phanit.khmersale.R;
import com.phanit.khmersale.app.BaseActivity;
import com.phanit.khmersale.constants.Constant;
import com.squareup.picasso.Picasso;

public class ArticleDetailsActivity extends BaseActivity  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_details);
        initBack();
        initView();
    }

    private void initView() {
        WebView webView = findViewById(R.id.webViewArticleDetail);
        ImageView imageView = findViewById(R.id.img_article_detail);
        webView.getSettings().setJavaScriptEnabled(true);


        Intent intent = getIntent();
        int id = intent.getIntExtra(Constant.ID,0);
        String title = intent.getStringExtra(Constant.TITLE);
        String description = intent.getStringExtra(Constant.DESCRIPTION);
        String imageUrl = intent.getStringExtra(Constant.IMAGE_URL);
        if (id != 0) {
            initAppBar(title);
            Picasso.get().load(Constant.BaseUrlImageView + imageUrl)
                    .error(R.drawable.no_image)
                    .placeholder(R.drawable.ic_placeholder)
                    .into(imageView);
        }
        // Load the HTML content
        String htmlContent = "<html><body>" + description + "</body></html>";
        webView.loadData(htmlContent, "text/html", "UTF-8");
    }
}