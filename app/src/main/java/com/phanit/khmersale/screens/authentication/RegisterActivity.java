package com.phanit.khmersale.screens.authentication;

import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.material.textfield.TextInputLayout;
import com.phanit.khmersale.R;
import com.phanit.khmersale.app.BaseActivity;
import com.phanit.khmersale.models.auth.LoginRes;
import com.phanit.khmersale.models.auth.Register;
import com.phanit.khmersale.presenters.LoginPresenter;
import com.phanit.khmersale.views.LoginView;

import java.util.Objects;

public class RegisterActivity extends BaseActivity implements LoginView {
    private EditText etUsername, etEmail, etPhone, etPassword, etConfirmPass;

    private Register register;
    private LoginPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initView();
        initProgressBar();
        initAppBar("Register Account");
        initBack();
    }

    private void initView() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        presenter = new LoginPresenter(this);
        register = new Register();

        etUsername = findViewById(R.id.edUsername);
        etEmail = findViewById(R.id.edEmail);
        etPhone = findViewById(R.id.edPhone);
        etPassword = findViewById(R.id.edPassword);
        etConfirmPass = findViewById(R.id.edConfirmPassword);


    }


    public void onClickRegister(View view) {

        if (etUsername.length() == 0){
            etUsername.requestFocus();
            etUsername.setError("Please input Username");
        } else if (etEmail.length() == 0) {
            etEmail.requestFocus();
            etEmail.setError("Please input Email");
        } else if (etPhone.length() == 0) {
            etPhone.requestFocus();
            etPhone.setError("Please input Phone");
        } else if (etPassword.length() == 0) {
            etPassword.requestFocus();
            etPassword.setError("Please input your Password");
        } else if (etConfirmPass.length() == 0) {
            etConfirmPass.requestFocus();
            etConfirmPass.setError("Please input Confirm Password");
        }
        else if (!etPassword.getText().toString().trim().equals(etConfirmPass.getText().toString().trim())) {
            etConfirmPass.requestFocus();
            etConfirmPass.setError("Passwords do not match");
        } else {
            register.setUsername(etUsername.getText().toString().trim());
            register.setEmail(etEmail.getText().toString().trim());
            register.setPhone(etPhone.getText().toString().trim());
            register.setPassword(etPassword.getText().toString().trim());
            presenter.createUser(register);
        }
    }
    @Override
    public void onLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHiding() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onSuccess(Object data) {
        showMessage("Success...!");
    }

    @Override
    public void onError(String message) {
        showMessage(message);
    }

    @Override
    public void onServerError(String message) {
        showMessage(message);
    }

    @Override
    public void onLoginSuccess(LoginRes res) {

    }

}