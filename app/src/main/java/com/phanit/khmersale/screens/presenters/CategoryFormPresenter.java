package com.phanit.khmersale.screens.presenters;

import android.content.Context;

import androidx.annotation.NonNull;

import com.phanit.khmersale.api.APIClient;
import com.phanit.khmersale.api.APIInterface;
import com.phanit.khmersale.models.BaseResponse;
import com.phanit.khmersale.models.Category;
import com.phanit.khmersale.screens.views.CategoryFormView;
import com.phanit.khmersale.utils.MultiPartHelper;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryFormPresenter {
    private final CategoryFormView view;
    private final APIInterface apiInterface;

    public CategoryFormPresenter(CategoryFormView view) {
        this.view = view;
        apiInterface = APIClient.getClientAPIs().create(APIInterface.class);
    }

    public void createCategory(Category req){
        view.onLoading();
        apiInterface.createCategory(req).enqueue(new Callback<BaseResponse<String>>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse<String>> call, @NonNull Response<BaseResponse<String>> response) {
                view.onHiding();
                if (response.isSuccessful() && response.body() != null){
                    view.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse<String>> call, @NonNull Throwable t) {
                view.onHiding();
                view.onError(t.getLocalizedMessage());
                view.onServerError("Connection Timeout");
            }
        });
    }
    public void updateCategory(Category req){
        view.onLoading();
        apiInterface.updateCategory(req).enqueue(new Callback<BaseResponse<String>>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse<String>> call, @NonNull Response<BaseResponse<String>> response) {
                view.onHiding();
                if (response.isSuccessful() && response.body() != null){
                    view.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse<String>> call, @NonNull Throwable t) {
                view.onHiding();
                view.onError(t.getLocalizedMessage());
                view.onServerError("Connection Timeout");
            }
        });
    }

    public void delCategory(Category req){
        view.onLoading();
        apiInterface.deleteCategory(req).enqueue(new Callback<BaseResponse<String>>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse<String>> call, @NonNull Response<BaseResponse<String>> response) {
                view.onHiding();
                if (response.isSuccessful() && response.body() != null){
                    view.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse<String>> call, @NonNull Throwable t) {
                view.onHiding();
                view.onError(t.getLocalizedMessage());
                view.onServerError("Connection Timeout");
            }
        });
    }

    public void uploadImage(String filePath, Context context){
        view.onLoading();
        try {
            MultipartBody.Part part = new MultiPartHelper().createPart(context, "file", filePath);
            apiInterface.uploadImage(part).enqueue(new Callback<BaseResponse<String>>() {
                @Override
                public void onResponse(@NonNull Call<BaseResponse<String>> call, @NonNull Response<BaseResponse<String>> response) {
                    view.onHiding();
                    if (response.isSuccessful() && response.body() != null) {
                        view.onUploadImageSuccess(response.body().getData());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<BaseResponse<String>> call, @NonNull Throwable t) {
                    view.onHiding();
                    view.onError(t.getLocalizedMessage());
                }
            });
        }catch (Throwable e){
            view.onHiding();
        }
    }


}
