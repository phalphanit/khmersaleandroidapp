package com.phanit.khmersale.screens.authentication;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.phanit.khmersale.MainActivity;
import com.phanit.khmersale.R;
import com.phanit.khmersale.app.BaseActivity;
import com.phanit.khmersale.models.User;
import com.phanit.khmersale.models.auth.LoginRes;
import com.phanit.khmersale.presenters.LoginPresenter;
import com.phanit.khmersale.screens.admin.dashboard.AdminDashBoardActivity;
import com.phanit.khmersale.utils.local.UserSharedPreference;
import com.phanit.khmersale.views.LoginView;

public class LoginActivity extends BaseActivity implements LoginView {
    private EditText etPhone, etPassword;
    private TextView signUp;
    private LoginPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initProgressBar();
        initView();
        initBack();
        initAppBar("Login");
    }
    @Override
    protected void onResume() {
        super.onResume();
        checkLogin();
    }

    private void initView() {
        presenter = new LoginPresenter(this);

        etPhone = findViewById(R.id.edPhone);
        etPassword = findViewById(R.id.edPassword);
        signUp = findViewById(R.id.tvGotoRegister);
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });
    }

    public void onClickLogin(View view) {
        String phone = etPhone.getText().toString().trim();
        String password = etPassword.getText().toString().trim();

        if (phone.equals("")) {
            etPhone.requestFocus();
            etPhone.setError("PHONE CAN'T BE EMPTY");
            return;
        }
        if (password.equals("")) {
            etPassword.requestFocus();
            etPassword.setError("PASSWORD CAN'T BE EMPTY");
            return;
        }
        presenter.login(phone, password);
    }


    @Override
    public void onLoading() {
        progressBar.setVisibility(View.VISIBLE);

    }

    @Override
    public void onHiding() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onSuccess(Object data) {

    }


    @Override
    public void onError(String message) {
        showMessage(message);
    }

    @Override
    public void onServerError(String message) {
        showMessage(message);
    }

    @Override
    public void onLoginSuccess(LoginRes res) {
        User user = new User();
        user.setId(1);
        user.setUsername(res.getUsername());
        user.setPassword(res.getAccessToken());
        user.setAccessToken(res.getAccessToken());
        UserSharedPreference.setUser(LoginActivity.this, user);
        Intent intent = new Intent(LoginActivity.this, AdminDashBoardActivity.class);
        startActivity(intent);
        showMessage("Login Success");
        finish();
    }

}