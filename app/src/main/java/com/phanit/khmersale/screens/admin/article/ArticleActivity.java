package com.phanit.khmersale.screens.admin.article;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.phanit.khmersale.R;
import com.phanit.khmersale.adapter.ArticleAdapter;
import com.phanit.khmersale.app.BaseActivity;
import com.phanit.khmersale.callback.CallBackListener;
import com.phanit.khmersale.constants.Constant;
import com.phanit.khmersale.models.Article;
import com.phanit.khmersale.screens.presenters.ArticlePresenter;
import com.phanit.khmersale.screens.views.ArticleView;
import com.phanit.khmersale.utils.local.UserSharedPreference;

import java.util.List;

public class ArticleActivity extends BaseActivity implements ArticleView {
    private RecyclerView recyclerView;
    private ArticleAdapter articleAdapter;
    private ArticlePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);
        initProgressBar();
        initView();
        initBack();
        initAppBar("Article");
        initSwipeRefresh();
    }

    private void initView() {
        recyclerView = findViewById(R.id.rcViewArticle);
        presenter = new ArticlePresenter(this);
        presenter.getAllArticle();
        swipeRefreshLayout = findViewById(R.id.swipeRefresh);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            presenter.getAllArticle();
            swipeRefreshLayout.setRefreshing(false);
        });
    }

    public void onClickOpenArticleForm(View view){
        Intent intent = new Intent(ArticleActivity.this, ArticleFormActivity.class);
        startActivity(intent);
    }

    @Override
    public void onLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHiding() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onSuccess(Object data) {
        if (data != null){
            List<Article> articleList = (List<Article>) data;
            articleAdapter = new ArticleAdapter(this, articleList, new CallBackListener() {
                @Override
                public void onClickCardItem(Object data, View view) {
                    Article article = (Article) data;
                    Intent intent = new Intent(ArticleActivity.this, ArticleFormActivity.class);
                    intent.putExtra(Constant.ID,article.getId());
                    startActivity(intent);
                }
            });
        }
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this,1,RecyclerView.VERTICAL,false);
        recyclerView.setAdapter(articleAdapter);
        recyclerView.setLayoutManager(gridLayoutManager);

    }

    @Override
    public void onError(String message) {
        showMessage(message);
    }

    @Override
    public void onServerError(String message) {
        showMessage(message);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.getAllArticle();
    }
}