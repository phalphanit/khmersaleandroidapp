//package com.phanit.khmersale.screens.user.fragment;
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ProgressBar;
//import android.widget.Toast;
//
//import androidx.fragment.app.Fragment;
//import androidx.recyclerview.widget.GridLayoutManager;
//import androidx.recyclerview.widget.RecyclerView;
//
//
//import com.phanit.khmersale.R;
//import com.phanit.khmersale.adapter.ArticleByCategoryIdFragmentAdapter;
//import com.phanit.khmersale.callback.CallBackListener;
//import com.phanit.khmersale.constants.Constant;
//import com.phanit.khmersale.models.Article;
//import com.phanit.khmersale.screens.user.ArticleDetailsActivity;
//import com.phanit.khmersale.screens.user.presenter.UsersPresenter;
//import com.phanit.khmersale.screens.user.view.UserView;
//import com.phanit.khmersale.utils.local.UserSharedPreference;
//
//import java.util.List;
//
//
//public class SportFragment extends Fragment implements UserView {
//    private RecyclerView recyclerView;
//    private ProgressBar progressBar;
//    private UsersPresenter presenter;
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.fragment_news, container, false);
//        recyclerView = view.findViewById(R.id.rcArticleByCategoryId);
//        presenter = new UsersPresenter(this);
//        progressBar = view.findViewById(R.id.ProgressBar);
//        //Sport
//        int categoryId = 3;
//        presenter.getArticleByCategoryId(categoryId);
//
//        return view;
//    }
//
//    @Override
//    public void onLoading() {
//        progressBar.setVisibility(View.VISIBLE);
//    }
//
//    @Override
//    public void onHiding() {
//        progressBar.setVisibility(View.GONE);
//    }
//
//    @Override
//    public void onError(String message) {
//        progressBar.setVisibility(View.GONE);
//    }
//
//    @Override
//    public void onServerError(String message) {
//
//    }
//
//    @Override
//    public void onSuccess(Object data) {
//
//    }
//
//    @Override
//    public void onGetArticleByCategoryId(Object data) {
//        List<Article> list = (List<Article>) data;
//        ArticleByCategoryIdFragmentAdapter adapter = new ArticleByCategoryIdFragmentAdapter(getContext(), list, new CallBackListener() {
//            @Override
//            public void onClickCardItem(Object data, View view) {
//                Article article = (Article) data;
//                Intent intent = new Intent(getActivity(), ArticleDetailsActivity.class);
//                intent.putExtra(Constant.ID,article.getId());
//                intent.putExtra(Constant.TITLE,article.getTitle());
//                intent.putExtra(Constant.DESCRIPTION,article.getDescription());
//                startActivity(intent);
//                presenter.getArticleId(article.getId());
//            }
//        });
//        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(),1,RecyclerView.VERTICAL,false);
//        recyclerView.setLayoutManager(gridLayoutManager);
//        recyclerView.setAdapter(adapter);
//    }
//
//    @Override
//    public void onGetArticleByIdSuccess(Article article) {
//        Toast.makeText(getContext(),article.getTitle(),Toast.LENGTH_LONG).show();
//    }
//}