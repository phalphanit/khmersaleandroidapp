package com.phanit.khmersale.screens.admin.category;


import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.phanit.khmersale.R;
import com.phanit.khmersale.adapter.CategoryAdapter;
import com.phanit.khmersale.app.BaseActivity;
import com.phanit.khmersale.callback.CallBackListener;
import com.phanit.khmersale.constants.Constant;
import com.phanit.khmersale.models.Category;
import com.phanit.khmersale.models.Slide;
import com.phanit.khmersale.screens.presenters.CategoryPresenter;
import com.phanit.khmersale.screens.views.CategoryView;
import com.phanit.khmersale.utils.local.UserSharedPreference;

import java.util.List;

public class CategoryActivity extends BaseActivity implements CategoryView {
    private RecyclerView rcCategory;
    private CategoryPresenter presenter;
    private CategoryAdapter adapter;
    public static int RESULT = 1;
    private final Context context = this;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        initProgressBar();
        initView();
        initBack();
        initSwipeRefresh();
        initAppBar("Category");
    }

    private void initView() {
        rcCategory = findViewById(R.id.recyclerCategory);
        swipeRefreshLayout = findViewById(R.id.swipeRefresh);
        presenter = new CategoryPresenter(this);
        presenter.getAllCategoryWithBaseRes();
        swipeRefreshLayout.setOnRefreshListener(() -> {
            presenter.getAllCategoryWithBaseRes();
            swipeRefreshLayout.setRefreshing(false);
        });

    }

    @Override
    public void onLoading() {
        progressBar.setVisibility(View.VISIBLE);

    }

    @Override
    public void onHiding() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onSuccess(Object data) {
        if (null != data){
            List<Category> list = (List<Category>) data;
            adapter = new CategoryAdapter(this, list, new CallBackListener() {
                @Override
                public void onClickCardItem(Object data, View view) {
                    Category category = (Category) data;
                    presenter.getCategoryId(category.getId());
                }
            });
            GridLayoutManager layoutManager = new GridLayoutManager(this,1,RecyclerView.VERTICAL,false);
            rcCategory.setAdapter(adapter);
            rcCategory.setLayoutManager(layoutManager);
        }
    }

    @Override
    public void onError(String message) {
        showMessage(message);
    }

    @Override
    public void onServerError(String message) {
        showMessage(message);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.getAllCategoryWithBaseRes();
    }

    public void onClickOpenCategoryForm(View view){
        Intent i = new Intent(CategoryActivity.this, CategoryFormActivity.class);
        startActivityForResult(i,RESULT);
    }

    @Override
    public void onGetCategoryById(Category category) {
        Intent intent = new Intent(CategoryActivity.this, CategoryFormActivity.class);
        intent.putExtra(Constant.ID, category.getId());
        intent.putExtra(Constant.NAME,category.getName());
        intent.putExtra(Constant.NAME_KH,category.getNameKh());
        intent.putExtra(Constant.IMAGE_URL,category.getImageUrl());
        intent.putExtra(Constant.STATUS,category.getStatus());
        startActivityForResult(intent,RESULT);
    }

    @Override
    public void onGetArticleSuccess(Object article) {

    }

    @Override
    public void onGetImageSuccess(List<Slide> image) {

    }
}