package com.phanit.khmersale.screens.views;

import com.phanit.khmersale.views.BaseView;

public interface CategoryFormView extends BaseView {
    void onUploadImageSuccess(String imageUrl);
}
