package com.phanit.khmersale.screens.views;

import com.phanit.khmersale.models.Category;
import com.phanit.khmersale.models.Slide;
import com.phanit.khmersale.views.BaseView;

import java.util.List;

public interface CategoryView extends BaseView {
    void onGetCategoryById(Category category);
    void onGetArticleSuccess(Object article);
    void onGetImageSuccess(List<Slide> imageUrl);
}
