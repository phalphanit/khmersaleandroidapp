package com.phanit.khmersale.screens.presenters;

import androidx.annotation.NonNull;

import com.phanit.khmersale.api.APIClient;
import com.phanit.khmersale.api.APIInterface;
import com.phanit.khmersale.models.Article;
import com.phanit.khmersale.models.BaseResponse;
import com.phanit.khmersale.screens.views.ArticleView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticlePresenter {
    private final ArticleView view;
    private final APIInterface api;

    public ArticlePresenter(ArticleView view) {
        this.view = view;
        api = APIClient.getClientAPIs().create(APIInterface.class);
    }

    public void getAllArticle(){
        view.onLoading();
        api.getAllArticle().enqueue(new Callback<BaseResponse<List<Article>>>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse<List<Article>>> call, @NonNull Response<BaseResponse<List<Article>>> response) {
                view.onHiding();
                if (response.isSuccessful() && response.body() != null){
                    view.onSuccess(response.body().getData());
                }
            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse<List<Article>>> call, @NonNull Throwable t) {
                view.onHiding();
                view.onError(t.getLocalizedMessage());
                view.onServerError("Connection time out");
            }
        });
    }
}
