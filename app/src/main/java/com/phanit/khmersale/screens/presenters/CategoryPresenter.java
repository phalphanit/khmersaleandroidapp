package com.phanit.khmersale.screens.presenters;

import androidx.annotation.NonNull;

import com.phanit.khmersale.api.APIClient;
import com.phanit.khmersale.api.APIInterface;
import com.phanit.khmersale.models.Article;
import com.phanit.khmersale.models.BaseResponse;
import com.phanit.khmersale.models.Category;
import com.phanit.khmersale.models.CategoryRes;
import com.phanit.khmersale.models.Slide;
import com.phanit.khmersale.screens.views.CategoryView;
import com.phanit.khmersale.utils.Utils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryPresenter {
    private final CategoryView view;
    private final APIInterface apisInterface;

    public CategoryPresenter(CategoryView view) {
        this.view = view;
        apisInterface = APIClient.getClientAPIs().create(APIInterface.class);
    }
    public void getAllArticle(){
        view.onLoading();
        apisInterface.getAllArticle().enqueue(new Callback<BaseResponse<List<Article>>>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse<List<Article>>> call, @NonNull Response<BaseResponse<List<Article>>> response) {
                view.onHiding();
                if (response.isSuccessful() && response.body() != null){
                    view.onGetArticleSuccess(response.body().getData());
                }
            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse<List<Article>>> call, @NonNull Throwable t) {
                view.onHiding();
                view.onError(t.getLocalizedMessage());
                view.onServerError("Connection time out");
            }
        });
    }

    public void getAllCategoryWithBaseRes(){
        view.onLoading();
        apisInterface.getAllCategoryWithBase().enqueue(new Callback<BaseResponse<List<Category>>>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse<List<Category>>> call, @NonNull Response<BaseResponse<List<Category>>> response) {
                view.onHiding();
                if (response.isSuccessful() && response.body() != null){
                    view.onSuccess(response.body().getData());
                }
            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse<List<Category>>> call, @NonNull Throwable t) {
                view.onHiding();
                view.onError(t.getLocalizedMessage());
                view.onServerError("Connection Timeout");
            }
        });
    }

    public void getCategoryId(int id){
        view.onLoading();
        apisInterface.getCategoryById(id).enqueue(new Callback<BaseResponse<Category>>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse<Category>> call, @NonNull Response<BaseResponse<Category>> response) {
                view.onHiding();
                if (response.code() == 404){
                    view.onError("Id not found...!");
                }
                if (response.isSuccessful() && response.body() != null){
                    view.onGetCategoryById(response.body().getData());
                }
            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse<Category>> call, @NonNull Throwable t) {
                view.onHiding();
                view.onServerError(t.getLocalizedMessage());
            }
        });
    }



    public void updateCategory(Category req){
        view.onLoading();
        apisInterface.updateCategory(req).enqueue(new Callback<BaseResponse<String>>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse<String>> call, @NonNull Response<BaseResponse<String>> response) {
                view.onHiding();
                if (response.isSuccessful()){
                    if (response.body() != null){
                        view.onSuccess(response.body());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse<String>> call, @NonNull Throwable t) {
                view.onHiding();
                view.onError(t.getLocalizedMessage());
            }
        });
    }

    public void getAllImage(){
        view.onLoading();
        apisInterface.getImageList().enqueue(new Callback<BaseResponse<List<Slide>>>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse<List<Slide>>> call, @NonNull Response<BaseResponse<List<Slide>>> response) {
                if (response.code() != 200){
                    view.onError("Error");
                }
                if (response.isSuccessful() && response.body() != null){
                    view.onGetImageSuccess(response.body().getData());
                }
            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse<List<Slide>>> call, @NonNull Throwable t) {
                view.onHiding();
                view.onError(t.getLocalizedMessage());
            }
        });
    }

}
