package com.phanit.khmersale.screens.presenters;

import android.content.Context;

import androidx.annotation.NonNull;

import com.phanit.khmersale.api.APIClient;
import com.phanit.khmersale.api.APIInterface;
import com.phanit.khmersale.models.Article;
import com.phanit.khmersale.models.BaseResponse;
import com.phanit.khmersale.models.Category;
import com.phanit.khmersale.screens.views.ArticleFormView;
import com.phanit.khmersale.utils.MultiPartHelper;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticleFormPresenter {
    private final ArticleFormView view;
    private final APIInterface apiInterface;

    public ArticleFormPresenter(ArticleFormView view) {
        this.view = view;
        apiInterface = APIClient.getClientAPIs().create(APIInterface.class);
    }

    public void getCategoryToSpinner(){
        view.onLoading();
        apiInterface.getAllCategoryWithBase().enqueue(new Callback<BaseResponse<List<Category>>>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse<List<Category>>> call, @NonNull Response<BaseResponse<List<Category>>> response) {
                view.onHiding();
                if (response.isSuccessful()){
                    if (response.body() != null){
                        view.onGetAllCategorySuccess(response.body().getData());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse<List<Category>>> call, @NonNull Throwable t) {
                view.onHiding();
                view.onError(t.getLocalizedMessage());
            }
        });
    }

    public void createArticle(Article req){
        view.onLoading();
        apiInterface.createArticle(req).enqueue(new Callback<BaseResponse<String>>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse<String>> call, @NonNull Response<BaseResponse<String>> response) {
                view.onHiding();
                if (response.isSuccessful()){
                    if (response.body() != null){
                        view.onSuccess(response.body());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse<String>> call, @NonNull Throwable t) {
                view.onHiding();
                view.onError(t.getLocalizedMessage());
            }
        });
    }

    public void getArticleById(int id){
        view.onLoading();
        apiInterface.getArticleById(id).enqueue(new Callback<BaseResponse<Article>>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse<Article>> call, @NonNull Response<BaseResponse<Article>> response) {
                view.onHiding();
                if (response.isSuccessful()){
                    if (response.body() != null){
                        view.onGetArticleIdSuccess(response.body().getData());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse<Article>> call, @NonNull Throwable t) {
                view.onHiding();
                view.onError(t.getLocalizedMessage());
            }
        });
    }

    public void updateArticle(Article req){
        view.onLoading();
        apiInterface.createArticle(req).enqueue(new Callback<BaseResponse<String>>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse<String>> call, @NonNull Response<BaseResponse<String>> response) {
                view.onHiding();
                if (response.isSuccessful()){
                    if (response.body() != null){
                        view.onSuccess(response.body());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse<String>> call, @NonNull Throwable t) {
                view.onHiding();
                view.onError(t.getLocalizedMessage());
            }
        });
    }


    public void uploadImage(String filePath, Context context){
        view.onLoading();
        try {
            MultipartBody.Part part = new MultiPartHelper().createPart(context, "file", filePath);
            apiInterface.uploadImage(part).enqueue(new Callback<BaseResponse<String>>() {
                @Override
                public void onResponse(@NonNull Call<BaseResponse<String>> call, @NonNull Response<BaseResponse<String>> response) {
                    view.onHiding();
                    if (response.isSuccessful()){
                        if (response.body() != null){
                            view.onUploadImageSuccess(response.body().getData());
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<BaseResponse<String>> call, @NonNull Throwable t) {
                    view.onHiding();
                    view.onError(t.getLocalizedMessage());
                }
            });
        }catch (Throwable e){
            view.onHiding();
        }
    }
}
