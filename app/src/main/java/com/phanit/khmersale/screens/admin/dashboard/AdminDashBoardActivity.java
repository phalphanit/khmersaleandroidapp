package com.phanit.khmersale.screens.admin.dashboard;

import androidx.cardview.widget.CardView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.phanit.khmersale.MainActivity;
import com.phanit.khmersale.R;
import com.phanit.khmersale.app.BaseActivity;
import com.phanit.khmersale.screens.admin.article.ArticleActivity;
import com.phanit.khmersale.screens.admin.category.CategoryActivity;
import com.phanit.khmersale.utils.local.UserSharedPreference;

import java.util.Objects;

public class AdminDashBoardActivity extends BaseActivity {
    private CardView cardView;
    private Dialog dialog;
    private Button showDialog, logOut, cancel;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_dash_board);
        initView();
        initBack();
        initAppBar("Admin Dashboard");

    }

    private void initView() {
        cardView = findViewById(R.id.cvCategory);
        showDialog = findViewById(R.id.btnLogout);
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.custom_dialog_layout);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(getDrawable(R.drawable.custom_dialog_bg));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false); //Optional
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation; //Setting the animations to dialog

        logOut = dialog.findViewById(R.id.btn_okay);
        cancel = dialog.findViewById(R.id.btn_cancel);

        logOut.setOnClickListener(v -> {
            UserSharedPreference.removeUser(AdminDashBoardActivity.this);
            Intent intent = new Intent(AdminDashBoardActivity.this, MainActivity.class);
            startActivity(intent);
            dialog.dismiss();
        });

        cancel.setOnClickListener(view -> {dialog.dismiss();});


        showDialog.setOnClickListener(view -> dialog.show());
        cardView.setOnClickListener(v -> {
            Intent i = new Intent(AdminDashBoardActivity.this, CategoryActivity.class);
            startActivity(i);
        });

    }

    public void onClickOpenArticle(View view) {
        Intent intent = new Intent(AdminDashBoardActivity.this, ArticleActivity.class);
        startActivity(intent);
    }

}