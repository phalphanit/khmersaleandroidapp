package com.phanit.khmersale.screens.admin.article;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import com.bumptech.glide.Glide;
import com.phanit.khmersale.MainActivity;
import com.phanit.khmersale.R;
import com.phanit.khmersale.adapter.BaseAdapterCategory;
import com.phanit.khmersale.app.BaseActivity;
import com.phanit.khmersale.constants.Constant;
import com.phanit.khmersale.models.Article;
import com.phanit.khmersale.models.Category;
import com.phanit.khmersale.screens.presenters.ArticleFormPresenter;
import com.phanit.khmersale.screens.user.ArticleDetailsActivity;
import com.phanit.khmersale.screens.views.ArticleFormView;
import com.phanit.khmersale.utils.CheckRuntimePermission;
import com.phanit.khmersale.utils.local.UserSharedPreference;

import java.util.ArrayList;
import java.util.List;

public class ArticleFormActivity extends BaseActivity implements ArticleFormView {
    private EditText title, titleKh, description, descriptionKh;
    private ImageView imageViewUpload;
    private Spinner spinnerCategory;
    private ArticleFormPresenter presenter;
    private Category category;
    private Article article;
    private static final String CHANNEL_ID = "Article Form";
    private static final int NOTIFICATION_ID = 1;

    private List<Category> categoryList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_form);
        initView();
        initBack();
    }

    private void initView() {
        new CheckRuntimePermission().grantWriteExternalStorage(this);
        initProgressBar();
        initRadioButtonActAndDel();

        spinnerCategory = findViewById(R.id.spinnerCategory);
        imageViewUpload = findViewById(R.id.ivUploadImage);

        presenter = new ArticleFormPresenter(this);
        presenter.getCategoryToSpinner();
        category = new Category();
        article = new Article();

        title = findViewById(R.id.etArticleTitle);
        titleKh = findViewById(R.id.etArticleTitleKh);
        description = findViewById(R.id.etArticleDescription);
        descriptionKh = findViewById(R.id.etArticleDescriptionKh);
        Button btnCreate = findViewById(R.id.btnCreateArticle);

        Intent intent = getIntent();
        int id = intent.getIntExtra(Constant.ID, 0);
        if (id == 0) {
            initAppBar("Create");
        }
        if (id != 0) {
            initAppBar("Update");
            presenter.getArticleById(id);
            btnCreate.setText(R.string.update);
        }
        imageViewUpload.setOnClickListener(v -> chooseImage());
        createNotificationChannel();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK) {
                assert data != null;
                Uri uri = data.getData();
                try {
                    String[] filePathColumns = {MediaStore.Images.Media.DATA};
                    assert uri != null;
                    Cursor cursor = getContentResolver().query(uri, filePathColumns, null, null, null);
                    assert cursor != null;
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumns[0]);
                    String filePath = cursor.getString(columnIndex);
                    cursor.close();
                    presenter.uploadImage(filePath, this);
                } catch (Throwable e) {
                    showMessage(e.getMessage());
                }

            }
        } catch (Throwable e) {
            showMessage(e.getMessage());
        }
    }

    @Override
    public void onLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHiding() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onError(String message) {
        showMessage(message);
    }

    @Override
    public void onServerError(String message) {

    }

    @Override
    public void onSuccess(Object data) {
        finish();
    }


    @Override
    public void onGetAllCategorySuccess(List<Category> list) {
        if (list.size() > 0) {
//            category = list.get(0);
            categoryList = list;
            BaseAdapterCategory baseAdapterCategory = new BaseAdapterCategory(this, list);
            spinnerCategory.setAdapter(baseAdapterCategory);
            spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    category = list.get(position);
//                    showMessage(category.getName());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    @Override
    public void onGetArticleIdSuccess(Article article) {
        if (article.getTitle() != null) {
            this.article = article;
            title.setText(article.getTitle());
            titleKh.setText(article.getTitleKh());
            description.setText(article.getDescription());
            descriptionKh.setText(article.getDescriptionKh());
            if (article.getStatus().equals(Constant.ACT)) {
                rbActive.setChecked(true);
            } else {
                rbDelete.setChecked(true);
            }
            if (article.getImageUrl() == null) {
                imageViewUpload.setImageResource(R.drawable.no_image);
            } else {
                Glide.with(this).load(Constant.BaseUrlImageView + article.getImageUrl())
                        .centerCrop()
                        .into(imageViewUpload);
            }

            category = article.getCategory();
            for (int i = 0; i < categoryList.size(); i++) {
                if (article.getCategory().getId() == categoryList.get(i).getId()) {
                    spinnerCategory.setSelection(i);
                    return;
                }
            }

        }
    }

    @Override
    public void onUploadImageSuccess(String imageUrl) {
        //for upload and view image when select upload from device
        article.setImageUrl(imageUrl);
        Glide.with(this).load(Constant.BaseUrlImageView + imageUrl)
                .centerCrop()
                .into(imageViewUpload);
    }

    public void onClickCreateArticle(View view) {
        article.setCategory(category);
        article.setTitle(title.getText().toString().trim());
        article.setTitleKh(titleKh.getText().toString().trim());
        article.setDescription(description.getText().toString().trim());
        article.setDescriptionKh(descriptionKh.getText().toString().trim());
        article.setStatus(rbActive.isChecked() ? "ACT" : "DEL");

        if (article.getId() == 0) {
            presenter.createArticle(article);
        }else {
            presenter.updateArticle(article);
        }
        showNotification();
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    CHANNEL_ID,
                    "My Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager notificationManager =
                    getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void showNotification() {
        // Create the intent to open ArticleDetailsActivity
        Intent intent = new Intent(this, ArticleDetailsActivity.class);
        intent.putExtra("articleId", article.getId());

        // Create the PendingIntent
        PendingIntent pendingIntent = PendingIntent.getActivity(
                this,
                0,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT
        );

        // Build the notification
        NotificationCompat.Builder notificationBuilder;
        if (article.getId() != 0) {
            notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_notifications)
                    .setContentTitle("Update")
                    .setContentText(article.getTitle())
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setContentIntent(pendingIntent) // Set the PendingIntent
                    .setAutoCancel(true);
        } else {
            notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_notifications)
                    .setContentTitle("Create")
                    .setContentText(article.getTitle())
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setContentIntent(pendingIntent) // Set the PendingIntent
                    .setAutoCancel(true);
        }

        // Display the notification
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.POST_NOTIFICATIONS) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            return;
        }
        notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
    }


}