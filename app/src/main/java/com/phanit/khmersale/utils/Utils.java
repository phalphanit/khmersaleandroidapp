package com.phanit.khmersale.utils;


import com.phanit.khmersale.api.APIClient;
import com.phanit.khmersale.api.APIInterface;

public class Utils {
    public static APIInterface getClientAPIs(){
        return APIClient.getClientAPIs().create(APIInterface.class);
    }
}
