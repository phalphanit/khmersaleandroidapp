package com.phanit.khmersale.utils.local;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.phanit.khmersale.constants.Constant;
import com.phanit.khmersale.models.User;

public class UserSharedPreference {


    public static void setUser(Context context, User user) {
        SharedPreferences.Editor editor = context.getSharedPreferences("USER", Context.MODE_PRIVATE).edit();
        editor.putInt(Constant.ID, user.getId());
        editor.putString(Constant.USERNAME, user.getUsername());
        editor.putString(Constant.PASSWORD, user.getPassword());
        editor.putString(Constant.ACCESS_TOKEN, user.getAccessToken());
        editor.apply();
    }

    public static User getUser(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("USER", Context.MODE_PRIVATE);
        int id = preferences.getInt(Constant.ID, 0);
        String username = preferences.getString(Constant.USERNAME, "");
        String password = preferences.getString(Constant.PASSWORD, "");
        String accessToken = preferences.getString(Constant.ACCESS_TOKEN, "");

        if (!username.equals("")) {
            User user = new User();
            user.setId(id);
            user.setUsername(username);
            user.setPassword(password);
            user.setAccessToken(accessToken);
            return user;
        }
        return null;
    }

    public static void removeUser(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences("USER", Context.MODE_PRIVATE).edit();
        editor.remove("ID");
        editor.remove("USERNAME");
        editor.remove("PASSWORD");
        editor.remove("ACCESS_TOKEN");
        editor.apply();
    }

    public static String getToken(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("USER", Context.MODE_PRIVATE);
        String accessToken = preferences.getString("ACCESS_TOKEN", "");
        return "Bearer " + accessToken;
    }

}
