package com.phanit.khmersale.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.phanit.khmersale.R;
import com.phanit.khmersale.callback.CallBackListener;
import com.phanit.khmersale.constants.Constant;
import com.phanit.khmersale.models.Category;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>   {

    private Context context;
    private List<Category> categoryList;
    private CallBackListener callBackListener;

    public CategoryAdapter(Context context, List<Category> categoryList) {
        this.context = context;
        this.categoryList = categoryList;
    }

    public CategoryAdapter(Context context, List<Category> categoryList, CallBackListener callBackListener) {
        this.context = context;
        this.categoryList = categoryList;
        this.callBackListener = callBackListener;
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.custom_list_layout,parent,false);
        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        Category object = categoryList.get(position);
        if (object != null){
            holder.name.setText(object.getName());
            holder.nameKh.setText(object.getNameKh());
            holder.status.setText(object.getStatus());
            holder.cardView.setOnClickListener(view -> callBackListener.onClickCardItem(object,view));
            if (object.getImageUrl() != null){
                Picasso.get().load(Constant.BaseUrlImageView + object.getImageUrl())
                        .error(R.drawable.no_image)
                        .placeholder(R.drawable.ic_placeholder)
                        .into(holder.imageView);
            }
        }
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder{
        private ImageView imageView;
        private TextView name, nameKh, status;
        private CardView cardView;
        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.ivCategory);
            name = itemView.findViewById(R.id.tvCategoryName);
            nameKh=itemView.findViewById(R.id.tvCategoryNameKh);
            status=itemView.findViewById(R.id.tvCategoryStatus);
            cardView = itemView.findViewById(R.id.cardViewCategory);
        }
    }
}
