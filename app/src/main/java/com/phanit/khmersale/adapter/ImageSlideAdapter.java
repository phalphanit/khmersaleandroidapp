package com.phanit.khmersale.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.viewpager.widget.PagerAdapter;

import com.phanit.khmersale.R;
import com.phanit.khmersale.constants.Constant;
import com.phanit.khmersale.models.Slide;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ImageSlideAdapter extends PagerAdapter {
    private Context context;
    private List<Slide> imageUrl;


    public ImageSlideAdapter(Context context, List<Slide> imageUrl) {
        this.context = context;
        this.imageUrl = imageUrl;

    }

    @Override
    public int getCount() {
        return imageUrl.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        Slide slide = imageUrl.get(position);
        ImageView imageView = new ImageView(context);
        if (null != imageUrl){
            Picasso.get().load(Constant.BaseUrlImageView + slide.getImageUrl())
                    .error(R.drawable.no_image)
                    .placeholder(R.drawable.ic_placeholder)
                    .into(imageView);
            container.addView(imageView);
        }
        return imageView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((ImageView) object);
    }
}
