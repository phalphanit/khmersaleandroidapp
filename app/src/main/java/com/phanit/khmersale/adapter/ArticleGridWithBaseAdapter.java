package com.phanit.khmersale.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.phanit.khmersale.R;
import com.phanit.khmersale.callback.CallBackListener;
import com.phanit.khmersale.constants.Constant;
import com.phanit.khmersale.models.Article;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ArticleGridWithBaseAdapter extends BaseAdapter {
    private Context context;
    private List<Article> articles;
    private CallBackListener callBackListener;

    public ArticleGridWithBaseAdapter(Context context, List<Article> articles) {
        this.context = context;
        this.articles = articles;
    }

    public ArticleGridWithBaseAdapter(Context context, List<Article> articles, CallBackListener callBackListener) {
        this.context = context;
        this.articles = articles;
        this.callBackListener = callBackListener;
    }

    @Override
    public int getCount() {
        return articles.size();
    }

    @Override
    public Object getItem(int position) {
        return articles.get(position);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.custom_grid_view_item, null);
        }
        CardView cardView = convertView.findViewById(R.id.img_article_grid);
        ImageView imageView = convertView.findViewById(R.id.img_grid_article);
        TextView title = convertView.findViewById(R.id.article_title);
        TextView description = convertView.findViewById(R.id.article_description);

        Article article = articles.get(i);
        Picasso.get().load(Constant.BaseUrlImageView + article.getImageUrl())
                .error(R.drawable.no_image)
                .placeholder(R.drawable.ic_placeholder)
                .into(imageView);
        title.setText(article.getTitle());
        description.setText(article.getDescription());

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callBackListener.onClickCardItem(article,view);
            }
        });

        return convertView;
    }
}
