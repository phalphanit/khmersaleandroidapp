package com.phanit.khmersale.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.phanit.khmersale.R;
import com.phanit.khmersale.callback.CallBackListener;
import com.phanit.khmersale.constants.Constant;
import com.phanit.khmersale.models.Article;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ArticleCategoryListAdapter extends RecyclerView.Adapter<ArticleCategoryListAdapter.ArticleCategoryViewHolder> {
    private Context context;
    private List<Article> articleList;
    private CallBackListener callBackListener;

    public ArticleCategoryListAdapter(Context context, List<Article> articleList) {
        this.context = context;
        this.articleList = articleList;
    }

    public ArticleCategoryListAdapter(Context context, List<Article> articleList, CallBackListener callBackListener) {
        this.context = context;
        this.articleList = articleList;
        this.callBackListener = callBackListener;
    }

    @NonNull
    @Override
    public ArticleCategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ArticleCategoryViewHolder(LayoutInflater.from(context).inflate(R.layout.category_article_card_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ArticleCategoryViewHolder holder, int position) {
        Article article = articleList.get(position);
        if (article != null) {
            Picasso.get().load(Constant.BaseUrlImageView + article.getImageUrl())
                    .error(R.drawable.no_image)
                    .placeholder(R.drawable.no_image)
                    .into(holder.imageView);
            holder.title.setText(article.getTitle());
            holder.description.setText(article.getDescription());
            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callBackListener.onClickCardItem(article,view);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return articleList.size();
    }

    static class ArticleCategoryViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView title, description;
        CardView cardView;

        public ArticleCategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.cv_category_article);
            imageView = itemView.findViewById(R.id.img_article);
            title = itemView.findViewById(R.id.article_title);
            description = itemView.findViewById(R.id.article_description);
        }
    }
}
