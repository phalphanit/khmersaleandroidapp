package com.phanit.khmersale.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.phanit.khmersale.R;
import com.phanit.khmersale.models.Category;

import java.util.List;

public class BaseAdapterCategory extends BaseAdapter {
    private Context context;
    private List<Category> categoryList;

    public BaseAdapterCategory(Context context, List<Category> categoryList) {
        this.context = context;
        this.categoryList = categoryList;
    }

    @Override
    public int getCount() {
        return categoryList.size();
    }

    @Override
    public Object getItem(int position) {
        return categoryList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return categoryList.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View currentView = convertView;
        if (currentView == null){
            currentView = LayoutInflater.from(context).inflate(R.layout.spinner_item_layout,parent,false);
            TextView name = currentView.findViewById(R.id.title);
            name.setText(categoryList.get(position).getName());
        }
        return currentView;
    }
}
