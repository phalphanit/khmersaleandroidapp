package com.phanit.khmersale.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.phanit.khmersale.R;
import com.phanit.khmersale.callback.CallBackListener;
import com.phanit.khmersale.constants.Constant;
import com.phanit.khmersale.models.Article;
import com.phanit.khmersale.models.Category;

import java.util.List;

public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.ArticleViewHolder> {
    private final Context context;
    private final List<Article> list;
    private CallBackListener callBackListener;

    public ArticleAdapter(Context context, List<Article> list) {
        this.context = context;
        this.list = list;
    }

    public ArticleAdapter(Context context, List<Article> list, CallBackListener callBackListener) {
        this.context = context;
        this.list = list;
        this.callBackListener = callBackListener;
    }

    @NonNull
    @Override
    public ArticleAdapter.ArticleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.custom_article_item_layout, parent, false);
        return new ArticleViewHolder(view);
    }



    @Override
    public void onBindViewHolder(@NonNull ArticleAdapter.ArticleViewHolder holder, int position) {
        Article obj = list.get(position);
        if (obj != null){
            holder.title.setText(obj.getTitle());
            holder.titleKh.setText(obj.getTitleKh());
            holder.des.setText(obj.getDescription());
            holder.desKh.setText(obj.getDescriptionKh());
            holder.category.setText(obj.getCategory().getName());
            if (obj.getImageUrl() != null){
                Glide.with(context).load(Constant.BaseUrlImageView + obj.getImageUrl())
                        .centerCrop()
                        .placeholder(R.drawable.ic_placeholder)
                        .into(holder.articleImage);
            }
            holder.cardViewArticle.setOnClickListener(v -> callBackListener.onClickCardItem(obj,v));
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ArticleViewHolder extends RecyclerView.ViewHolder {
        private final TextView title, titleKh, des, desKh, category;
        private final CardView cardViewArticle;
        private final ImageView articleImage;
        public ArticleViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.tvArticleTitle);
            titleKh = itemView.findViewById(R.id.tvArticleTitleKh);
            des = itemView.findViewById(R.id.tvArticleDescription);
            desKh = itemView.findViewById(R.id.tvArticleDescriptionKh);
            category = itemView.findViewById(R.id.tvCategory);
            cardViewArticle = itemView.findViewById(R.id.cardViewArticle);
            articleImage = itemView.findViewById(R.id.ivArticle);
        }
    }
}
