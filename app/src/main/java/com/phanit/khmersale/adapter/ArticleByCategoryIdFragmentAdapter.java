//package com.phanit.khmersale.adapter;
//
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import androidx.annotation.NonNull;
//import androidx.cardview.widget.CardView;
//import androidx.recyclerview.widget.RecyclerView;
//
//import com.bumptech.glide.Glide;
//import com.phanit.khmersale.R;
//import com.phanit.khmersale.callback.CallBackListener;
//import com.phanit.khmersale.constants.Constant;
//import com.phanit.khmersale.models.Article;
//
//import java.util.List;
//
//public class ArticleByCategoryIdFragmentAdapter extends RecyclerView.Adapter<ArticleByCategoryIdFragmentAdapter.ViewHolder>{
//    private final   Context context;
//    private final   List<Article> articles;
//    private CallBackListener callBackListener;
//
//    public ArticleByCategoryIdFragmentAdapter(Context context, List<Article> articles) {
//        this.context = context;
//        this.articles = articles;
//    }
//
//    public ArticleByCategoryIdFragmentAdapter(Context context, List<Article> articles, CallBackListener callBackListener) {
//        this.context = context;
//        this.articles = articles;
//        this.callBackListener = callBackListener;
//    }
//
//    @NonNull
//    @Override
//    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.artice_category_fragment_layout, parent, false));
//    }
//
//    @Override
//    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
//        Article article = articles.get(position);
//        if (article != null){
//            holder.articleTitle.setText(article.getTitle());
//            holder.articleTitleKh.setText(article.getTitleKh());
//            holder.description.setText(article.getDescription());
//            if (article.getImageUrl() != null){
//                Glide.with(context).load(Constant.BaseUrlImageView + article.getImageUrl())
//                        .centerCrop()
//                        .into(holder.fragmentImage);
//            }
//
//            holder.cardViewItem.setOnClickListener(v -> callBackListener.onClickCardItem(article,v));
//        }
//    }
//
//    @Override
//    public int getItemCount() {
//        return articles.size();
//    }
//
//
//    public static class ViewHolder extends RecyclerView.ViewHolder{
//        private final TextView articleTitle, articleTitleKh, description;
//        private final ImageView fragmentImage;
//        private final CardView cardViewItem;
//        public ViewHolder(@NonNull View itemView) {
//            super(itemView);
//            articleTitle = itemView.findViewById(R.id.tvTitle);
//            articleTitleKh = itemView.findViewById(R.id.tvTitleKh);
//            description = itemView.findViewById(R.id.tvArticleDes);
//            fragmentImage = itemView.findViewById(R.id.ivArticleFragment);
//            cardViewItem = itemView.findViewById(R.id.cvArticleItem);
//        }
//    }
//}
