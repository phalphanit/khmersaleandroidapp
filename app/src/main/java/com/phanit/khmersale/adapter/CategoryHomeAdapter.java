package com.phanit.khmersale.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.phanit.khmersale.R;
import com.phanit.khmersale.callback.CallBackListener;
import com.phanit.khmersale.models.Category;

import java.util.List;

public class CategoryHomeAdapter extends RecyclerView.Adapter<CategoryHomeAdapter.CategoryViewHolder>{
    List<Category> list;
    Context context;
    CallBackListener callBackListener;

    public CategoryHomeAdapter(List<Category> list, Context context) {
        this.list = list;
        this.context = context;
    }

    public CategoryHomeAdapter(List<Category> list, Context context, CallBackListener callBackListener) {
        this.list = list;
        this.context = context;
        this.callBackListener = callBackListener;
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CategoryViewHolder(LayoutInflater.from(context).inflate(R.layout.category_home_screen_card,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        Category category = list.get(position);
        if (category != null){
            holder.name.setText(category.getName());
            holder.name.setOnClickListener(view -> callBackListener.onClickCardItem(category,view));
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class CategoryViewHolder extends RecyclerView.ViewHolder{
        Button name;
        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.btn_category_name);
        }
    }
}
