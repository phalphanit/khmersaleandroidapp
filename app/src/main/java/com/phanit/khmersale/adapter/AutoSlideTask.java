package com.phanit.khmersale.adapter;

import androidx.viewpager.widget.ViewPager;

import java.util.Objects;
import java.util.TimerTask;

public class AutoSlideTask extends TimerTask {
    private ViewPager viewPager;

    public AutoSlideTask(ViewPager viewPager) {
        this.viewPager = viewPager;
    }

    @Override
    public void run() {
        viewPager.post(() -> {
            if (viewPager.getAdapter() != null) {
                int currentItem = viewPager.getCurrentItem();
                int totalItems = viewPager.getAdapter().getCount();
                int nextPage = (currentItem + 1) % totalItems;
                viewPager.setCurrentItem(nextPage, true);
            }
        });
    }
}
