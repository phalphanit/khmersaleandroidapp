//package com.phanit.khmersale.adapter;
//
//import androidx.annotation.NonNull;
//import androidx.fragment.app.Fragment;
//import androidx.viewpager2.adapter.FragmentStateAdapter;
//
//import com.phanit.khmersale.models.Category;
//import com.phanit.khmersale.screens.user.TabViewActivity;
//import com.phanit.khmersale.screens.user.fragment.AutoTalkFragment;
//import com.phanit.khmersale.screens.user.fragment.HealthFragment;
//import com.phanit.khmersale.screens.user.fragment.NewsFragment;
//import com.phanit.khmersale.screens.user.fragment.SportFragment;
//
//import java.util.List;
//
//public class MyViewPagerAdapter extends FragmentStateAdapter {
//
//    List<Category> categoryList;
//    public MyViewPagerAdapter(@NonNull TabViewActivity fragment) {
//        super(fragment);
//    }
//
//    @NonNull
//    @Override
//    public Fragment createFragment(int position) {
//        switch (position){
//            case 1:
//                return new SportFragment();
//            case 2:
//                return new NewsFragment();
//            case 3:
//                return new HealthFragment();
//            default:
//                return new AutoTalkFragment();
//        }
//    }
//
//    @Override
//    public int getItemCount() {
//        return 4;
//    }
//}
