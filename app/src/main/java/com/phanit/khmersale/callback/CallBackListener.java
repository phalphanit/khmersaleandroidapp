package com.phanit.khmersale.callback;

import android.view.View;

public interface CallBackListener {
    void onClickCardItem(Object data, View view);
}
