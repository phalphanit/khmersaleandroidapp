package com.phanit.khmersale;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.Switch;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.navigation.NavigationView;
import com.phanit.khmersale.adapter.ArticleGridWithBaseAdapter;
import com.phanit.khmersale.adapter.AutoSlideTask;
import com.phanit.khmersale.adapter.CategoryHomeAdapter;
import com.phanit.khmersale.adapter.ImageSlideAdapter;
import com.phanit.khmersale.adapter.SliderAdapter;
import com.phanit.khmersale.app.BaseActivity;
import com.phanit.khmersale.callback.CallBackListener;
import com.phanit.khmersale.constants.Constant;
import com.phanit.khmersale.models.Article;
import com.phanit.khmersale.models.Category;
import com.phanit.khmersale.models.Slide;
import com.phanit.khmersale.models.SliderData;
import com.phanit.khmersale.models.User;
import com.phanit.khmersale.screens.authentication.LoginActivity;
import com.phanit.khmersale.screens.presenters.CategoryPresenter;
import com.phanit.khmersale.screens.user.ArticleDetailsActivity;
import com.phanit.khmersale.screens.user.CategoryArticleList;
import com.phanit.khmersale.screens.views.CategoryView;
import com.phanit.khmersale.utils.local.UserSharedPreference;
import com.smarteist.autoimageslider.SliderView;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

public class MainActivity extends BaseActivity implements CategoryView {
    private RecyclerView recyclerViewCategory;
    CategoryHomeAdapter adapter;
    GridView gridViewArticle;
    ArticleGridWithBaseAdapter gridWithBaseAdapter;

    NavigationView navigationView;
    ActionBarDrawerToggle actionBarDrawerToggle;
    DrawerLayout drawerLayout;
    Toolbar toolbar;
    private CategoryPresenter presenter;
    private Switch switchMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initProgressBar();
        initView();
        initSwipeRefresh();

    }

    private void initView() {
        presenter = new CategoryPresenter(this);
        presenter.getAllCategoryWithBaseRes();
        presenter.getAllArticle();
        presenter.getAllImage();

        recyclerViewCategory = findViewById(R.id.recycler_view_category);
        gridViewArticle = findViewById(R.id.gridView_article);
        swipeRefreshLayout = findViewById(R.id.swipeRefresh);
        navigationView = findViewById(R.id.navigation_view);
        drawerLayout = findViewById(R.id.drawer_layout);
        switchMode = findViewById(R.id.switch_DarkLightMode);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //toggle button
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        navigationView.setNavigationItemSelectedListener(item -> {
            int itm = item.getItemId();

            if (itm == R.id.home) {
                drawerLayout.closeDrawer(GravityCompat.START);
            }
            if (itm == R.id.login) {
                drawerLayout.closeDrawer(GravityCompat.START);
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
            }
            return true;
        });

        swipeRefreshLayout.setOnRefreshListener(() -> {
            presenter.getAllCategoryWithBaseRes();
            presenter.getAllArticle();
            swipeRefreshLayout.setRefreshing(false);
        });

        switchMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                } else {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                }
            }
        });

        //Image Slide
        ArrayList<SliderData> sliderDataArrayList = new ArrayList<>();
        SliderView sliderView = findViewById(R.id.image_slide);
        sliderDataArrayList.add(new SliderData(Constant.url1));
        sliderDataArrayList.add(new SliderData(Constant.url2));
        sliderDataArrayList.add(new SliderData(Constant.url3));
        SliderAdapter adapter = new SliderAdapter(this, sliderDataArrayList);
        sliderView.setAutoCycleDirection(SliderView.LAYOUT_DIRECTION_LTR);
        sliderView.setSliderAdapter(adapter);
        sliderView.setScrollTimeInSec(4);
        sliderView.setAutoCycle(true);
        sliderView.startAutoCycle();


    }


    @Override
    public void onGetCategoryById(Category category) {

    }

    @Override
    public void onGetArticleSuccess(Object article) {
        List<Article> list = (List<Article>) article;
        List<Article> subList = list.subList(0,4);

        gridWithBaseAdapter = new ArticleGridWithBaseAdapter(this, subList, new CallBackListener() {
            @Override
            public void onClickCardItem(Object data, View view) {
                Article article = (Article) data;
                Intent intent = new Intent(MainActivity.this, ArticleDetailsActivity.class);
                intent.putExtra(Constant.ID, article.getId());
                intent.putExtra(Constant.TITLE, article.getTitle());
                intent.putExtra(Constant.DESCRIPTION, article.getDescription());
                intent.putExtra(Constant.IMAGE_URL, article.getImageUrl());
                startActivity(intent);
            }
        });
        gridViewArticle.setAdapter(gridWithBaseAdapter);
    }

    @Override
    public void onGetImageSuccess(List<Slide> imageUrls) {

    }


    @Override
    public void onLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHiding() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onSuccess(Object data) {
        List<Category> categoryList = (List<Category>) data;
        adapter = new CategoryHomeAdapter(categoryList, this, new CallBackListener() {
            @Override
            public void onClickCardItem(Object data, View view) {
                Category category = (Category) data;
                Intent intent = new Intent(MainActivity.this, CategoryArticleList.class);
                intent.putExtra(Constant.ID, category.getId());
                intent.putExtra(Constant.NAME, category.getName());
                presenter.getCategoryId(category.getId());
                startActivity(intent);
            }
        });
        GridLayoutManager layoutManager = new GridLayoutManager(this, 1, LinearLayoutManager.HORIZONTAL, false);
        recyclerViewCategory.setLayoutManager(layoutManager);
        recyclerViewCategory.setAdapter(adapter);

    }

    @Override
    public void onError(String message) {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onServerError(String message) {

    }
}