package com.phanit.khmersale.views;

import com.phanit.khmersale.models.auth.LoginRes;

public interface LoginView extends BaseView{
    void onLoginSuccess(LoginRes res);
}
