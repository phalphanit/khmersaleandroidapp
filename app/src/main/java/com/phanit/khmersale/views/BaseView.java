package com.phanit.khmersale.views;

public interface BaseView {
    void onLoading();
    void onHiding();
    void onSuccess(Object data);
    void onError(String message);
    void onServerError(String message);

}
