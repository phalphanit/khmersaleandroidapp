package com.phanit.khmersale.presenters;

import androidx.annotation.NonNull;

import com.phanit.khmersale.api.APIClient;
import com.phanit.khmersale.api.APIInterface;
import com.phanit.khmersale.models.auth.LoginReq;
import com.phanit.khmersale.models.auth.LoginRes;
import com.phanit.khmersale.models.auth.Register;
import com.phanit.khmersale.models.auth.RegisterRes;
import com.phanit.khmersale.utils.Utils;
import com.phanit.khmersale.views.LoginView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresenter {
    private final LoginView view;
    private  APIInterface apiInterface;

    public LoginPresenter(LoginView view) {
        this.view = view;
        apiInterface = APIClient.getClientAPIs().create(APIInterface.class);
    }

    public void login(String username, String password){
        view.onLoading();
        LoginReq req = new LoginReq();
        req.setPhone(username);
        req.setPassword(password);

        Call<LoginRes> loginResCall = Utils.getClientAPIs().log(req);
        loginResCall.enqueue(new Callback<LoginRes>() {
            @Override
            public void onResponse(@NonNull Call<LoginRes> call, @NonNull Response<LoginRes> response) {
                view.onHiding();
                if (response.code() == 401 ){
                    view.onError("Your username and password is incorrect");
                }
                if (response.isSuccessful() && null != response.body()){
                    view.onLoginSuccess(response.body());
                    view.onSuccess("Login Success");
                }
            }

            @Override
            public void onFailure(@NonNull Call<LoginRes> call, @NonNull Throwable t) {
                view.onHiding();
                view.onError(t.getLocalizedMessage());
                view.onServerError("Your Connection timeout!");
            }
        });
    }

    public void createUser(Register req){
        view.onLoading();
        apiInterface.register(req).enqueue(new Callback<RegisterRes>() {
            @Override
            public void onResponse(@NonNull Call<RegisterRes> call, @NonNull Response<RegisterRes> response) {
                view.onHiding();
                if (response.code() == 400){
                    view.onError("ERROR : Username or Email or Phone is already taken!");
                }

                if (response.isSuccessful() && response.body() != null){
                    view.onSuccess(response.body().getData());
                }
            }

            @Override
            public void onFailure(@NonNull Call<RegisterRes> call, @NonNull Throwable t) {
                view.onHiding();
                view.onError(t.getLocalizedMessage());
                view.onServerError("Your Connection timeout!");
            }
        });
    }

}
