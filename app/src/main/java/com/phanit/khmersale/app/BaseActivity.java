package com.phanit.khmersale.app;

import android.content.Intent;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.phanit.khmersale.R;
import com.phanit.khmersale.screens.admin.dashboard.AdminDashBoardActivity;
import com.phanit.khmersale.utils.local.UserSharedPreference;

import java.util.Objects;

public class BaseActivity extends AppCompatActivity {
    protected ProgressBar progressBar;
    protected ImageView imageViewBack;
    protected SwipeRefreshLayout swipeRefreshLayout;
    protected Switch switchMode;

    protected RadioButton rbActive, rbDelete;
    protected static final int PICK_IMAGE_REQUEST = 1;

    protected void initAppBar(String title){
        androidx.appcompat.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false); // Hide the default title
        LayoutInflater inflater = LayoutInflater.from(this);
        View customTitleView = inflater.inflate(R.layout.toolbar_custom_title, toolbar, false);
        ((TextView) customTitleView).setText(title);
        toolbar.addView(customTitleView); // Add the custom title view to the Toolbar
    }

    protected void initSwipeRefresh(){
        swipeRefreshLayout = findViewById(R.id.swipeRefresh);
    }

    protected void initProgressBar(){
        progressBar = findViewById(R.id.ProgressBar);
    }

    protected void initBack(){
        imageViewBack = findViewById(R.id.ivBack);
        imageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    protected void initRadioButtonActAndDel(){
        rbActive = findViewById(R.id.rbArticleAct);
        rbDelete = findViewById(R.id.rbArticleDel);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
    protected void checkLogin(){
        if (null != UserSharedPreference.getUser(this)) {
            Intent intent = new Intent(this, AdminDashBoardActivity.class);
            startActivity(intent);
            finish();
        }

    }

    protected void showMessage(String message){
        Toast.makeText(this,message,Toast.LENGTH_LONG).show();
    }

    protected void chooseImage() {
        try {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType("image/*");
            startActivityForResult(intent, PICK_IMAGE_REQUEST);
        } catch (Throwable e) {
            Toast.makeText(this, "Error choose image: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


}
