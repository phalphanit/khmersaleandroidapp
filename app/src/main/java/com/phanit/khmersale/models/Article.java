package com.phanit.khmersale.models;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Article {
    @SerializedName("id")
    private int id;
    @SerializedName("title")
    private String title;
    @SerializedName("titleKh")
    private String titleKh;
    @SerializedName("imageUrl")
    private String imageUrl;
    @SerializedName("description")
    private String description;
    @SerializedName("descriptionKh")
    private String descriptionKh;
    @SerializedName("category")
    private Category category;
    @SerializedName("createDate")
    private Date createDate;
    @SerializedName("createBy")
    private String createBy;
    @SerializedName("update")
    private String update;
    @SerializedName("updateBy")
    private String updateBy;
    @SerializedName("status")
    private String status;

    public Article() {
    }

    public Article(int id, String title, String titleKh, String imageUrl, String description, String descriptionKh, Category category, Date createDate, String createBy, String update, String updateBy, String status) {
        this.id = id;
        this.title = title;
        this.titleKh = titleKh;
        this.imageUrl = imageUrl;
        this.description = description;
        this.descriptionKh = descriptionKh;
        this.category = category;
        this.createDate = createDate;
        this.createBy = createBy;
        this.update = update;
        this.updateBy = updateBy;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleKh() {
        return titleKh;
    }

    public void setTitleKh(String titleKh) {
        this.titleKh = titleKh;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionKh() {
        return descriptionKh;
    }

    public void setDescriptionKh(String descriptionKh) {
        this.descriptionKh = descriptionKh;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getUpdate() {
        return update;
    }

    public void setUpdate(String update) {
        this.update = update;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
