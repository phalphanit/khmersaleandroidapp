package com.phanit.khmersale.constants;

public interface Constant {
    String ID = "ID";
    String NAME = "NAME";
    String NAME_KH = "NAME_KH";
    String IMAGE_URL = "IMAGE_URL";
    String STATUS = "STATUS";
    String USERNAME = "USERNAME";
    String PASSWORD = "PASSWORD";
    String ACCESS_TOKEN = "ACCESS_TOKEN";

    String TITLE = "TITLE";
    String DESCRIPTION = "DESCRIPTION";
    String url1 = "https://hips.hearstapps.com/hmg-prod/images/911-gt3-rs-side-1676907438.jpeg?crop=0.588xw:0.441xh;0.231xw,0.309xh&resize=980:*";
    String url2 = "https://hips.hearstapps.com/hmg-prod/images/my22-hypersonic-1619122058.jpg?crop=0.553xw:0.369xh;0.243xw,0.417xh&resize=980:*";
    String url3 = "https://hips.hearstapps.com/hmg-prod/images/2023-chevrolet-corvette-z06-101-1664801423.jpg?crop=0.599xw:0.449xh;0.264xw,0.355xh&resize=980:*";

    String ACT = "ACT";
    String DEL = "DEL";

    String BASE_URL = "http://172.20.10.4:8080";
//    192.168.1.250 172.20.10.4
//    String BaseUrlImageView = BASE_URL + "/app/api/image/view?filename=";
    String BaseUrlImageView = BASE_URL + "/api/public/image/view?filename=";
}
